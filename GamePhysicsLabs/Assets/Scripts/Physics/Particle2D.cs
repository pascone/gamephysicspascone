﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Particle2D : MonoBehaviour
{
    public Slider slider;
    public Dropdown dropDown;

    //Step 1
    public Vector2 position, velocity, acceleration;

    public float rotation, rotationVelocity, rotationAcceleration;

    public int integrationType;

    //Step 2
    void UpdatePositionExplicitEuler(float dt)
    {
        // x(t+dt) = x(t) + v(t)dt
        //Euler's Method:
        //F(t+dt) = F(t) + f(t)dt
        //               + (dF/dt)dt
        position += velocity * dt;

        //v(t+dt) = v(t) + a(t)dt
        velocity += acceleration * dt;
    }

    void UpdatePositionKinematic(float dt)
    {
        position += velocity * dt + 0.5f * acceleration * dt * dt;

        velocity += acceleration * dt;
    }

    void UpdateRotationExplicitEuler(float dt)
    {
        rotation += rotationVelocity * dt;

        rotationVelocity += rotationAcceleration * dt;
    }

    void UpdateRotationKinematic(float dt)
    {
        rotation += rotationVelocity * dt + 0.5f * rotationAcceleration * dt * dt;

        rotationVelocity += rotationAcceleration * dt;
    }

    void Start()
    {
        integrationType = 0;
    }

    void FixedUpdate()
    {
        //Step 3
        //Integrate
        if (integrationType == 0)
        {
            UpdatePositionExplicitEuler(Time.fixedDeltaTime);
            UpdateRotationExplicitEuler(Time.fixedDeltaTime);
        }
        else
        {
            UpdatePositionKinematic(Time.fixedDeltaTime);
            UpdateRotationKinematic(Time.fixedDeltaTime);
        }
        //Apply to transform
        transform.position = position;

        //Step 4
        //test
        acceleration.x = -Mathf.Sin(Time.time);
        transform.eulerAngles = new Vector3(0, 0, rotation);
    }

    public void Slider()
    {
        rotationAcceleration = slider.value;
    }

    public void DropDown()
    {
        integrationType = dropDown.value;
    }
}
